window.addEventListener('load', function() {
	//stran nalozena

	//Posodobi opomnike
	var posodobiOpomnike = function() {
		var opomniki = document.querySelectorAll(".opomnik");

		for (var i = 0; i < opomniki.length; i++) {
			var opomnik = opomniki[i];
			var casovnik = opomnik.querySelector("span");
			var cas = parseInt(casovnik.innerHTML);
			if (cas == 0){
				window.alert("Opomnik!\n\nZadolžitev "+ naziv_opomnika.value +" je potekla!");
			} else {
				cas-=1;
				casovnik.innerHTML = ""+cas+"";
			}
		}
	}
	//Prijava
	var prijavniGumb = document.querySelector("#prijavniGumb");
	prijavniGumb.addEventListener("click", function(){
		var uporabnisko_ime = document.querySelector("#uporabnisko_ime").value;
		document.getElementById("uporabnik").innerHTML += uporabnisko_ime;
		document.querySelector(".pokrivalo").style.visibility = "hidden";
	});
	
	//Dodajanje opomnika
	var dodajGumb = document.getElementById("dodajGumb");
	dodajGumb.addEventListener("click", function() {
		var naziv_opomnika=document.getElementById("naziv_opomnika");
		var cas_opomnika=document.getElementById("cas_opomnika");
		document.getElementById("opomniki").innerHTML += 
		"<div class='opomnik'><div class='naziv_opomnika'>" + naziv_opomnika.value + 
		"</div><div class='cas_opomnika'> Opomnik čez <span>" + cas_opomnika.value + "</span> sekund.</div></div>";
		document.querySelector("#naziv_opomnika").innerHTML="";
		document.querySelector("#cas_opomnika").innerHTML="";
	});
	
	setInterval(posodobiOpomnike, 1000);

});
